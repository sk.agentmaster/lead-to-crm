import $ from 'jquery';
import 'bootstrap/js/dist/collapse';
import '../scss/options.scss';

$(document).ready(function () {

$('#bitrix-portal-input').change(function() { 
    let bitrixIdVal=$(this).val().replace('https://', '').replace('http://', '');
    let lic_plugin_link="https://"+bitrixIdVal+"/marketplace/detail/agent5.leadsincrm/";
    $(".install-lcm").attr('href',lic_plugin_link);
    chrome.storage.sync.set({
                "bitrixId": bitrixIdVal,
                "lic_plugin_link" : lic_plugin_link
            });
});
    let jquery_body = $('body');
    let userId;
    jquery_body.on('submit', 'form.bitrix-form', function () {
        if($(".lead-confirm").prop("checked")){
        
            $(".alert-danger").hide();
            let bitrixIdVal = $(this).find('.bitrix').val().replace('https://', '').replace('http://', '');
            let lead_email = $(this).find('.lead-email').val();
            let lead_name = $(this).find('.lead-name').val();
            let lead_phone = $(this).find('.lead-phone').val();
            let lead_confirm = $(this).find('.lead-confirm').val();
            chrome.storage.sync.set({
                "bitrixId": bitrixIdVal,
                "lic_email" : lead_email,
                "lic_name" : lead_name,
                "lic_phone" : lead_phone,
                "lic_confirm" : lead_confirm
            });
            $.ajax({
                url: 'https://api.leads-in-crm.ru/bitrix',
                data: {'user-id': userId, 'domain': bitrixIdVal, 'lead-email':lead_email,'lead-name':lead_name,'lead-phone':lead_phone,'lead-confirm':lead_confirm},
                method: 'POST',
                success: function (data, textStatus, jqXHR) {
                    if (data.result) {
                        return;
                    }
                    let app_client=data.appId;
                    $.ajax({
                        url:'https://' + bitrixIdVal + '/oauth/authorize/',
                        method: 'GET',
                        data: {'client_id':  data.appId , 'state': userId},
                        success: function (data, textStatus, jqXHR) {
                            if(data.includes('Application not installed')) {
                                chrome.storage.sync.set({
                                    "lic_alert_info" : 'visible'
                                });
                                $(".alert-info").show();
                                $(".alert-danger").html("На портале "+bitrixIdVal+" не установлен плагин Lead-In-CRM. Необходимо <a href='https://"+bitrixIdVal+"/marketplace/detail/agent5.leadsincrm/' target='_blank'>установить плагин</a> и повторно сохранить настройки").show();

                                // console.log("На портале "+bitrixIdVal+" не установлен плагин Lead-In-CRM. Необходимо <a href='https://"+bitrixIdVal+"/marketplace/detail/agent5.leadsincrm/' target='_blank'>установить плагин</a>");
                            } else {
                                $(".alert-info").hide();
                                chrome.storage.sync.set({
                                    "lic_alert_info" : 'hidden'
                                });
                                window.open(
                                    'https://' + bitrixIdVal + '/oauth/authorize/?client_id=' + app_client + '&state=' + userId//,
    //                                "socialPopupWindow",
    //                                "resizable,scrollbars,status"
    //"location=no,width=600,height=600,scrollbars=yes,top=100,left=700,resizable = no"
                                );
                            }
                          
                        },
                        error: function( jqXHR, textStatus, errorThrown ){
                            
                            if(jqXHR.responseText.includes("Этот домен отключен или не существует")) {
                                $(".alert-danger").html("Этот домен отключен или не существует. Проверьте, пожалуйста, возможно, есть ошибка или опечатка в адресе.").show();
                            } else {
                                $(".alert-danger").html("Ошибка: "+errorThrown).show();
                            }
                            console.log(jqXHR);
                        }
                        
                        
                        
                    });
                    
                }

            });

            return false;
        
         } else {
             alert('Вы не приняли условия использования плагина.');
             return false;
         }
    });

    chrome.storage.sync.get({
        "bitrixId": '',
        "lic_email" : '',
        "lic_name" : '',
        "lic_phone" : '',
        "lic_confirm" : '',
        "lic_plugin_link" :'https://www.bitrix24.ru/apps/?app=agent5.leadsincrm',
        "lic_alert_info":'',
        userId: ''
    }, function (items) {
        $(".install-lcm").attr('href',items.lic_plugin_link);
        if(items.lic_alert_info=='hidden') $(".alert-info").hide();
        $('.bitrix').val(items.bitrixId);
        $('.lead-email').val(items.lic_email),$('.lead-name').val(items.lic_name),$('.lead-phone').val(items.lic_phone),$('.lead-confirm').val(items.lic_confirm);
        if (items.userId === '') {
            items.userId = guid();
            chrome.storage.sync.set({userId: items.userId});
        }
        userId = items.userId;
        $('.user-id').text(userId);
        $("input[name='user-id']").val(userId);
    });
    let cssUrl = chrome.extension.getURL('css/options.scss');

});

function guid() {
    return s4() + '' + s4() + '' + s4() + '' + s4() + '' +
        s4() + '' + s4() + '' + s4() + '' + s4();
}

function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);
}