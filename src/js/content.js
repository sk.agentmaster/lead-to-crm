import serialize from 'form-serialize';
import isNumeric from 'is-numeric';
import '../scss/style.scss';

//const Raven = require('raven-js');

//Raven.config('https://eaa65b45c3b34b31bb64245718306f64@sentry.io/1298978').install();
//Raven.context(function () {

    const modalUrl = chrome.extension.getURL('html/modal.html'),
        $body = document.querySelector('body'),
        $sync_modal_outer = document.createElement('div');

    $sync_modal_outer.id = 'agent5-sync-modal-outer';
    $sync_modal_outer.classList.add('agent5-sync-modal-outer');

    let $current_dialog;

    fetch(modalUrl)
        .then(function (response) {
            return response.text();
        })
        .then(function (body) {
            $body.appendChild($sync_modal_outer);
            $sync_modal_outer.innerHTML = body;
        });


    chrome.extension.onMessage.addListener(function (msg, sender, sendResponse) {
        window.msg = msg;
        switch (msg.action) {
            case  "open_dialog_box":
                openDialogBox(msg);
                break;
            case "open_dialog_box_main":
                openDialogBoxMain(msg);
                break;
        }
        sendResponse('ok');

    });

    function setDialogMain(logins = {}) {
        let buttons = {};
        if (logins.bitrixLogin) {
            buttons["Отправить в Битрикс24"] = function () {
                let form_data = serialize($current_dialog.querySelector('form'));
                let button = this;

                button.style.pointerEvents = 'none';

                fetch(
                    "https://api.leads-in-crm.ru/bitrix/create",
                    {
                        method: 'POST',
                        headers: {
                            'Accept': 'application/json',
                            'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                        },
                        body: form_data
                    }
                )
                    .then(response => {
                        return response.json()
                    })
                    .then(data => {
                        let message_text = '';
                        if (isNumeric(data.result)) {
                            message_text = '<div id="lcrm-ok">Лид создан! Номер в битрикc24: '
                                + data.result
                                + '<br><a class="lcrm_btn" target="_blank" href="https://'
                                + logins.bitrixLogin
                                + '/crm/lead/details/'
                                + data.result
                                + '/">Перейти</a></div>';
                        } else {
                            message_text = 'Что-то пошло не так <br>' + data;
                            console.log(data);
                           // Raven.captureMessage(data.message);
                        }
                        $current_dialog.querySelector('.agent5-sync-modal-inner').innerHTML = (message_text);
                        button.style.pointerEvents = '';

                    }).catch(error => {
                        $current_dialog.querySelector('.agent5-sync-modal-inner').innerHTML = ('Error: Что-то пошло не так <br>'+error);
                        button.style.pointerEvents = '';
                      //  Raven.captureException(error);

                    }
                );
            };
        }

        return $current_dialog = setDialog("open", {
            title: "Создать лид",
            width: 400,
            height: 570,
            content: $sync_modal_outer.innerHTML,
            "buttons": buttons
        });
    }

    function openDialogBox(msg) {
        const $dialog = openDialogBoxMain(msg);

        $dialog.querySelector('input[name="phone"]').value = msg.selected;

    }

    function openDialogBoxMain(msg) {
        const $dialog = setDialogMain(msg.buttons);
        if (msg.selectors) {
            $dialog.querySelector('input[name="company"]').value = (oneOf(msg.selectors.name.first, msg.selectors.name.second));
            $dialog.querySelector('input[name="address"]').value = (oneOf(msg.selectors.address.first, msg.selectors.address.second)).replace('Адрес:','');;
            if(!!msg.selectors.email) $dialog.querySelector('input[name="email"]').value = (oneOf(msg.selectors.email.first, msg.selectors.email.second));
            $dialog.querySelector('input[name="web"]').value = (oneOf(msg.selectors.web.first, msg.selectors.web.second));
            if (!msg.selected) {
                $dialog.querySelector('input[name="phone"]').value = (oneOf(msg.selectors.phone.first, msg.selectors.phone.second)).replace('Телефон:','');
            }
        }

        $dialog.querySelector('input[name="from"]').value = (msg.url);
        $dialog.querySelector('input[name="user-id"]').value = (msg.bitrixId);

        return $dialog;
    }

    function oneOf(first, second = false) {
        let node_list;
        let one_of_result = false;
        
        if ((undefined!=first)&&(''!=first)&&(node_list = document.querySelectorAll(first)).length) {
            one_of_result = node_list[0];
        } else if ((undefined!=first)&&(''!=second)&&(node_list = document.querySelectorAll(second)).length) {
            one_of_result = node_list[0];
        }

        if (one_of_result) {
            return one_of_result.innerText;
        }

        return '';
    }

    function addListeners() {
        $sync_modal_outer.addEventListener('mousedown', mouseDown, false);
        window.addEventListener('mouseup', mouseUp, false);
    }

    function mouseUp() {
        window.removeEventListener('mousemove', divMove, true);
    }

    function mouseDown(e) {
        window.addEventListener('mousemove', divMove, true);
    }

    function divMove(e) {
        let div = document.getElementById('agent5-sync-modal-outer');
        div.style.top = e.clientY + 'px';
        div.style.left = e.clientX + 'px';
    }

    let uniqueId = new Date().getTime();
    (function () { // Create the dialog box markup
        let div = document.createElement('div'),
            ovr = document.createElement('div');
        div.className = 'dialog-box';
        div.id = 'dialog-box-' + uniqueId;
        div.innerHTML = '<h3 class="dialog-title">&nbsp;</h3><a href="javascript:;" class="dialog-close" title="Close">&times;</a><div class="dialog-content">&nbsp;</div><div class="dialog-action"></div>';
        ovr.className = 'dialog-box-overlay';
        document.body.appendChild(div);
        document.body.appendChild(ovr);
    })();
    let dialog = document.getElementById('dialog-box-' + uniqueId), // The HTML of dialog box
        dialog_title = dialog.children[0],
        dialog_close = dialog.children[1],
        dialog_content = dialog.children[2],
        dialog_action = dialog.children[3],
        dialog_overlay = dialog.nextSibling;
    window.setDialog = function (set, config) {
        let selected = null, // Object of the element to be moved
            x_pos = 0,
            y_pos = 0, // Stores x & y coordinates of the mouse pointer
            x_elem = 0,
            y_elem = 0, // Stores top, left values (edge) of the element
            defaults = {
                title: dialog_title.innerHTML,
                content: dialog_content.innerHTML,
                width: 300,
                height: 150,
                top: false,
                left: false,
                buttons: {
                    "Close": function () {
                        setDialog('close');
                    }
                },
                specialClass: "",
                fixed: true,
                overlay: false
            }; // Default options...

        for (let i in config) {
            defaults[i] = (typeof (config[i]))
                ? config[i]
                : defaults[i];
        }

        // Will be called when user starts dragging an element
        function _drag_init(elem) {
            selected = elem; // Store the object of the element which needs to be moved
            x_elem = x_pos - selected.offsetLeft;
            y_elem = y_pos - selected.offsetTop;
        }

        // Will be called when user dragging an element
        function _move_elem(e) {
            x_pos = document.all
                ? window.event.clientX
                : e.pageX;
            y_pos = document.all
                ? window.event.clientY
                : e.pageY;
            if (selected !== null) {
                selected.style.left = !defaults.left
                    ? ((x_pos - x_elem) + selected.offsetWidth / 2) + 'px'
                    : ((x_pos - x_elem) - defaults.left) + 'px';
                selected.style.top = !defaults.top
                    ? ((y_pos - y_elem) + selected.offsetHeight / 2) + 'px'
                    : ((y_pos - y_elem) - defaults.top) + 'px';
            }
        }

        // Destroy the object when we are done
        function _destroy() {
            selected = null;
        }

        dialog.className = "agent5-sync-modal dialog-box " + (defaults.fixed
            ? 'fixed-dialog-box '
            : '') + defaults.specialClass;
        dialog.style.visibility = (set === "open")
            ? "visible"
            : "hidden";
        dialog.style.opacity = (set === "open")
            ? 1
            : 0;
        dialog.style.width = defaults.width + 'px';
        dialog.style.height = defaults.height + 'px';
        dialog.style.top = (!defaults.top)
            ? "50%"
            : '0px';
        dialog.style.left = (!defaults.left)
            ? "50%"
            : '0px';
        dialog.style.marginTop = (!defaults.top)
            ? '-' + defaults.height / 2 + 'px'
            : defaults.top + 'px';
        dialog.style.marginLeft = (!defaults.left)
            ? '-' + defaults.width / 2 + 'px'
            : defaults.left + 'px';
        dialog_title.innerHTML = defaults.title;
        dialog_content.innerHTML = defaults.content;
        dialog_action.innerHTML = "";
        dialog_overlay.style.display = (set === "open" && defaults.overlay)
            ? "block"
            : "none";
        if (defaults.buttons) {
            for (let j in defaults.buttons) {
                let btn = document.createElement('a');
                btn.className = 'btn';
                btn.href = 'javascript:;';
                btn.innerHTML = j;
                btn.onclick = defaults.buttons[j];
                dialog_action.appendChild(btn);
            }
        } else {
            dialog_action.innerHTML = '&nbsp;';
        }

        // Bind the draggable function here...
        dialog_title.onmousedown = function () {
            _drag_init(this.parentNode);
            return false;
        };
        dialog_close.onclick = function () {
            setDialog("close", {content: ""});
        };
        document.onmousemove = _move_elem;
        document.onmouseup = _destroy;
        return dialog;
    };

//});