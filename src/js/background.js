// A generic onclick callback function.
function genericOnClick(info, tab) {
    getVals();
    currentSelectors = getSelector(info);
    chrome.tabs.query({active: true, currentWindow: true}, function () {
        chrome.tabs.sendMessage(tab.id, {
            action: "open_dialog_box",
            selected: info.selectionText,
            url: tab.url,
            "bitrixId": userId,
            selectors: currentSelectors,
            buttons: {amoLogin: amo, bitrixLogin: bitrixIdVal}
        }, function (response) {
            
        });
    });
}

function pageOnClick(info, tab) {
    getVals();
    currentSelectors = getSelector(info);
    chrome.tabs.query({active: true, currentWindow: true}, function () {
        chrome.tabs.sendMessage(tab.id, {
            action: "open_dialog_box_main",
            url: tab.url,
            "bitrixId": userId,
            selectors: currentSelectors,
            buttons: {amoLogin: amo, bitrixLogin: bitrixIdVal}
        }, function (response) {
        });
    });
}


function getSelector(info) {

    let currentSelectors = {
        name: {
            first: 'h1',
            second: 'h1'
        },
        address: {
            first: '',
            second: ''
        },
        phone: {
            first: "a[href^='tel:']",
            second: '.fa-phone ~ a'
        },
        email: {
          first:"a[href^='mailto:']",
          second:'.fa-envelope ~ a'
        },
        web: {
            first: '.fa-globe ~ a',
            second: ''
        }
    };

    if (info.pageUrl){


        if (/2gis\.ru/.test(info.pageUrl)) {
            currentSelectors = gis;
        } else if (/yandex\..{2,6}\/maps/.test(info.pageUrl)) {
            currentSelectors = ymaps;
        } else if (/google\..{2,6}\/maps/.test(info.pageUrl)) {
            currentSelectors = gmaps;
        } else if (/asktel\.ru/.test(info.pageUrl)) {
            currentSelectors = asktel;
        }  else if (/blizko\.ru/.test(info.pageUrl)) {
            currentSelectors = blizko;
        }
    }

    return currentSelectors;

}

// Create one test item for each context type.
var contexts = ["selection"];
var title = "Создать лид";
var id = chrome.contextMenus.create({
    "title": title, "contexts": ["selection"],
    "onclick": genericOnClick
});

chrome.contextMenus.create({'title': 'Создать лид', 'contexts': ['page'], 'onclick': pageOnClick});

bitrixIdVal = '';
amo = '';
userId = '';

function getVals() {
    chrome.storage.sync.get({
        "bitrixId": '',
        "userId": '',
        "amoLogin": ''
    }, function (items) {
        bitrixIdVal = items.bitrixId;
        userId = items.userId;
        amo = items.amoLogin;
    });
}


var gis = {
    name: {
        first: 'h1 span._oqoid',
        second: 'h1'
    },
    address: {
        first: 'a[href^="/ekaterinburg/geo/"]',
        second: '.card__addressLink'
    },
    phone: {
        first: "a[href^='tel:']",
        second: '.contact__phonesItemLinkNumber'
    },
    web: {
        first: "div._49kxlr span div a[href^='http://link.2gis.ru']",
        second: '.contact__linkText'
    },
    email: {
        first: "a[href^='mailto:']",
        second:'.email'
    }
};

var gmaps = {
    name: {
        first: 'h1.section-hero-header-title-title',
        second: 'h1'
    },
    address: {
        first: 'div[data-section-id="ad"] span.widget-pane-link',
        second: ''
    },
    phone: {
        first: 'div[data-section-id="pn0"] span.widget-pane-link',
        second: ''
    },
    web: {
        first: 'div[data-section-id="ap"] span.widget-pane-link',
        second: ''
    }
    
}


var ymaps = {
    name: {
        first: '.card-title-view__title-link',
        second: 'h1'
    },
    address: {
        first: '.business-card-view__address',
        second: ''
    },
    phone: {
        first: '.card-phones-view__phone-number',
        second: ''
    },
    web: {
        first: '.business-urls-view__url span:last-of-type',
        second: ''
    }
};

var asktel = {
    name: {
        first: 'h1[itemprop="name"]',
        second: 'h1'
    },
    address: {
        first: '.endCompanyAddress',
        second: ''
    },
    phone: {
        first: '.endCompanyPhone',
        second: ''
    },
    email: {
        first:'.endCompanyEmail a',
        second:''
    },
    web: {
        first: '.endCompanySite a',
        second: ''
    }
};


var blizko = {
    name: {
        first: 'a.ppsi-company',
        second: 'h1'
    },
    address: {
        first: 'div.short-address-',
        second: ''
    },
    phone: {
        first: "a[href^='tel:']",
        second: ''
    },
    email: {
        first:'li.email-:first-child a',
        second:''
    }
};


//chrome.runtime.onMessage.addListener(function (message, sender, sendResponse) {
    //amoKey: key.text(), amoLogin: login, amoHash: window.location	console.log(message);
   // console.log(message);

    /*chrome.storage.sync.set({
        "amoLogin": message.amoLogin,
        "amoSubdomain": message.amoAddress,
        "amoHash": message.amoKey
    });*/
//});